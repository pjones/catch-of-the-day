import Rebase from 're-base';
// import firebase from 'firebase';
const firebase = require('firebase/app');
// require('firebase/auth');
require('firebase/database');

const firebaseApp = firebase.initializeApp({
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: 'https://catch-of-the-day-2e8ed.firebaseio.com'
});

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp };

export default base;
